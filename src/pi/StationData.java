package pi;

class StationData {
    private int STN;
    private String DATE;
    private String TIME;
    private double TEMP;
    private double DEWP;
    private double STP;
    private double SLP;
    private double VISIB;
    private double WDSP;
    private double PRCP;
    private double SNDP;
    private int FRSHTT;
    private double CLDC;
    private int WNDDIR;

    int getSTN() {
        return STN;
    }

    void setSTN(int STN) {
        this.STN = STN;
    }

    String getDATE() {
        return DATE;
    }

    void setDATE(String DATE) {
        this.DATE = DATE;
    }

    String getTIME() {
        return TIME;
    }

    void setTIME(String TIME) {
        this.TIME = TIME;
    }

    double getTEMP() {
        return TEMP;
    }

    void setTEMP(double TEMP) {
        this.TEMP = TEMP;
    }

    double getDEWP() {
        return DEWP;
    }

    void setDEWP(double DEWP) {
        this.DEWP = DEWP;
    }

    double getSTP() {
        return STP;
    }

    void setSTP(double STP) {
        this.STP = STP;
    }

    double getSLP() {
        return SLP;
    }

    void setSLP(double SLP) {
        this.SLP = SLP;
    }

    double getVISIB() {
        return VISIB;
    }

    void setVISIB(double VISIB) {
        this.VISIB = VISIB;
    }

    double getWDSP() {
        return WDSP;
    }

    void setWDSP(double WDSP) {
        this.WDSP = WDSP;
    }

    double getPRCP() {
        return PRCP;
    }

    void setPRCP(double PRCP) {
        this.PRCP = PRCP;
    }

    double getSNDP() {
        return SNDP;
    }

    void setSNDP(double SNDP) {
        this.SNDP = SNDP;
    }

    int getFRSHTT() {
        return FRSHTT;
    }

    void setFRSHTT(int FRSHTT) {
        this.FRSHTT = FRSHTT;
    }

    double getCLDC() {
        return CLDC;
    }

    void setCLDC(double CLDC) {
        this.CLDC = CLDC;
    }

    int getWNDDIR() {
        return WNDDIR;
    }

    void setWNDDIR(int WNDDIR) {
        this.WNDDIR = WNDDIR;
    }
}