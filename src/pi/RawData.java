package pi;

import java.util.LinkedList;

class RawData {
    private LinkedList<String> list = new LinkedList<>();


    void addLine (String line){
        list.addLast(line);
    }

    String getLine(){
        return list.pollFirst();
    }
}
