package pi;

class ThreadLimiter {

    private int value;
    private int max;

    ThreadLimiter(int limit){
        this.max = limit;
        this.value = 0;
    }

    void increment(){
        value++;
    }

    void decrement(){
        value--;
    }

    boolean check(){
        return (value < max);
    }

}
