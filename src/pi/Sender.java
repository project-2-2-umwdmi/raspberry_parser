package pi;

import java.io.PrintWriter;
import java.net.Socket;

class Sender implements Runnable {

    private String hostName;
    private int port;
    private boolean exit;
    private int count;
    private String buffer;

    Sender(String hostName, int port){
        this.hostName = hostName;
        this.port = port;
        this.exit = true;
        this.count = 0;
        this.buffer = "";
    }

    public void run() {
        try {
            while (exit) {
                StationData next = Main.SendQueue.pollFromList();
                if (next == null) {
                    continue;
                }
                String data = String.format("%d;%s;%s;%.1f;%.1f;%.1f;%.1f;%.1f;%.1f;%.2f;%.1f;%d;%.1f;%d",
                        next.getSTN(), next.getDATE(), next.getTIME(), next.getTEMP(),
                        next.getDEWP(), next.getSTP(), next.getSLP(), next.getVISIB(),
                        next.getWDSP(), next.getPRCP(), next.getSNDP(), next.getFRSHTT(),
                        next.getCLDC(), next.getWNDDIR());
                if (this.count != 0){
                    this.buffer = this.buffer.concat("&v[]=");
                }
                this.buffer = this.buffer.concat(data);
                this.count++;
                if (!this.exit || this.count >= 10) {
                    Socket connection = new Socket(hostName, port);
                    PrintWriter out = new PrintWriter(connection.getOutputStream(), true);
                    out.println("GET /?v[]=" + this.buffer +" HTTP/1.0\n\n\n");
                    connection.close();
                    this.count = 0;
                    this.buffer = "";
                }
            }
        }
        catch (java.io.IOException ignored) {}
    }
}
