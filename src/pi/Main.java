package pi;

import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    private static final int Port = 7789;
    private static final int maxnrofConnections=800;
    private static final int outPort = 80;
    private static final String outHostName= "api.thebiglan.nl";

    static ThreadLimiter Limiter = new ThreadLimiter(maxnrofConnections);
    static RawDataQueue rawDataQueue = new RawDataQueue();
    static SendQueue SendQueue = new SendQueue();
    static ValidationQueueHandler ValidationQueueHandler = new ValidationQueueHandler();

    public static void main(String[] args) {
        Socket connection;
        try {
            Thread ParserWorker = new Thread(new ParserWorker());
            ParserWorker.start();

            Thread Sender = new Thread(new Sender(outHostName, outPort));
            Sender.start();

            ServerSocket receiver = new ServerSocket(Port);
            while (true) {
                connection = receiver.accept();
                if(!Limiter.check()) {
                    continue;
                }
                Thread dataWorker = new Thread(new DataWorker(connection));
                dataWorker.start();
                Limiter.increment();
            }
        }

        catch (java.io.IOException ignored) { }
    }
}