package pi;

import java.net.*;
import java.io.*;

class DataWorker implements Runnable {
    private Socket connection;

    DataWorker(Socket connection) {
        this.connection = connection;
    }

    public void run() {
        try {
            BufferedReader bin = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            RawData rd = new RawData();
            String s;

            while ((s = bin.readLine()) != null) {
                if (s.contains("/MEASUREMENT")) {
                    Main.rawDataQueue.addToList(rd);
                    rd = new RawData();
                    continue;
                }
                rd.addLine(s);
            }
            connection.close();
            Main.Limiter.decrement();
        }
        catch (IOException ignored) { }
    }
}
