package pi;

import java.util.HashMap;

class ValidationQueueHandler {

    private HashMap<Integer, ValidationData> ValidationDataSet = new HashMap<>();

    ValidationQueueHandler() {

    }

    void addToValidationData(int id, StationData sd) {
        if (!ValidationDataSet.containsKey(id)) {
            ValidationData Queue = new ValidationData();
            ValidationDataSet.put(id,Queue);
        }
        ValidationData Queue = ValidationDataSet.get(id);
        Queue.PushToStack(sd);
    }

    double getAvg(int id) {
        if(!ValidationDataSet.containsKey(id)){
            return 10000;
        }
        return (ValidationDataSet.get(id).getAverage());
    }

    double getPreviousDEWP(int id){
        return (ValidationDataSet.get(id).getDEWP());
    }

    double getPreviousSTP(int id){
        return (ValidationDataSet.get(id).getSTP());
    }

    double getPreviousSLP(int id){
        return (ValidationDataSet.get(id).getSLP());
    }

    double getPreviousVISIB(int id){
        return (ValidationDataSet.get(id).getVISIB());
    }

    double getPreviousWDSP(int id){
        return (ValidationDataSet.get(id).getWDSP());
    }

    double getPreviousPRCP(int id){
        return (ValidationDataSet.get(id).getPRCP());
    }

    double getPreviousSNDP(int id){
        return (ValidationDataSet.get(id).getSNDP());
    }

    int getPreviousFRSHTT(int id){
        return (ValidationDataSet.get(id).getFRSHTT());
    }

    double getPreviousCLDC(int id){
        return (ValidationDataSet.get(id).getCLDC());
    }

    int getPreviousWNDDIR(int id){
        return (ValidationDataSet.get(id).getWNDDIR());
    }
}
