package pi;

import java.util.LinkedList;

class RawDataQueue {

    private LinkedList<RawData> dataList = new LinkedList<>();

    RawDataQueue() {

    }

    synchronized void addToList(RawData rd) {
        dataList.addFirst(rd);
    }

    synchronized RawData popFromList() {
        return dataList.pollLast();
    }
}
