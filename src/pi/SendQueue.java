package pi;

import java.util.LinkedList;

class SendQueue {

    private LinkedList<StationData> dataList = new LinkedList<>();

    SendQueue() {

    }

    synchronized void pushToList(StationData sd) {
        dataList.addFirst(sd);
    }

    synchronized StationData pollFromList() {
        return dataList.pollLast();
    }
}
