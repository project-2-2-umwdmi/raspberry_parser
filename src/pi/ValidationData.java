package pi;

class ValidationData {

    private int listSize = 0;
    private int index = 0;
    private double[] TEMParray =  new double[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    private double DEWP;
    private double STP;
    private double SLP;
    private double VISIB;
    private double WDSP;
    private double PRCP;
    private double SNDP;
    private int FRSHTT;
    private double CLDC;
    private int WNDDIR;


    ValidationData(){

    }

    void PushToStack(StationData sd) {
        TEMParray[index] = sd.getTEMP();
        if (listSize == 29){
            if (index == 29){
                index = 0;
            } else {
                index++;
            }
        } else {
            listSize++;
            index++;
        }
        DEWP = sd.getDEWP();
        STP = sd.getSTP();
        SLP = sd.getSLP();
        VISIB = sd.getVISIB();
        WDSP = sd.getWDSP();
        PRCP = sd.getPRCP();
        SNDP = sd.getSNDP();
        FRSHTT = sd.getFRSHTT();
        CLDC = sd.getCLDC();
        WNDDIR = sd.getWNDDIR();
    }

    double getDEWP() {
        return DEWP;
    }

    double getSTP() {
        return STP;
    }

    double getSLP() {
        return SLP;
    }

    double getVISIB() {
        return VISIB;
    }

    double getWDSP() {
        return WDSP;
    }

    double getPRCP() {
        return PRCP;
    }

    double getSNDP() {
        return SNDP;
    }

    int getFRSHTT() {
        return FRSHTT;
    }

    double getCLDC() {
        return CLDC;
    }

    int getWNDDIR() {
        return WNDDIR;
    }

    double getAverage(){
        double total = 0;
        for (int i=0; i <= listSize; i++){
            double TEMP = TEMParray[i];
            total += TEMP;
        }
        return (double) Math.round((total/listSize) * 10) / 10;
    }
}