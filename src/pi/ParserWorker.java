package pi;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

class ParserWorker implements Runnable {

    private StationData sd;
    private int id;

    public void run() {
        while (true) {
            String s, m1, m2, m3;
            RawData rawData = Main.rawDataQueue.popFromList();
            this.id = -1;
            this.sd = new StationData();

            while (rawData == null) {
                try {
                    Thread.sleep(100);
                    rawData = Main.rawDataQueue.popFromList();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }

            while ((s = rawData.getLine()) != null) {
                Matcher matcher = Pattern.compile(".*<([A-Z]+)>([0-9-.:]+)</([A-Z]+)>.*").matcher(s);
                if (!matcher.matches()) {
                    continue;
                }
                m1 = matcher.group(1);
                m2 = matcher.group(2);
                m3 = matcher.group(3);
                if (!m1.equals(m3)) {
                    continue;
                }
                resolve(m1, m2);
            }
            validate();
            if (id != -1) {
                Main.SendQueue.pushToList(sd);
                Main.ValidationQueueHandler.addToValidationData(id, sd);
            }
        }
    }

    private void validate(){
        int STN = sd.getSTN();
        if (STN < 100000 || STN > 999999){
            this.id = -1;
            return;
        }

        if (!Pattern.compile("^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$").matcher(sd.getDATE()).matches()){
            this.id = -1;
            return;
        }
        if (!Pattern.compile("^(20|21|22|23|[01]\\d|\\d)((:[0-5]\\d){1,2})((:[0-5]\\d){1,2})$").matcher(sd.getTIME()).matches()) {
            this.id = -1;
            return;
        }
        // Check if temperature is inside the 20% rule.
        // If the temp is between 5 and -5 degrees celsius, check if it is in a 3 degrees margin of the average instead.
        double average = Main.ValidationQueueHandler.getAvg(STN);
        double TEMP = sd.getTEMP();
        if(average !=10000 && (!((TEMP > average*0.8 && average*1.2  > TEMP) ||
                (5 > TEMP && TEMP > -5 && TEMP > average-3 && average+3 > TEMP)))){
            sd.setTEMP(average);
        }
        if (sd.getDEWP() < -9999.9  || sd.getDEWP() > 9999.9) {
            sd.setDEWP(Main.ValidationQueueHandler.getPreviousDEWP(STN));
        }
        if (sd.getSTP() < 0  || sd.getSTP() > 9999.9) {
            sd.setSTP(Main.ValidationQueueHandler.getPreviousSTP(STN));
        }
        if (sd.getSLP() < 0  || sd.getSLP() > 9999.9) {
            sd.setSLP(Main.ValidationQueueHandler.getPreviousSLP(STN));
        }
        if (sd.getVISIB() < 0  || sd.getVISIB() > 999.9) {
            sd.setVISIB(Main.ValidationQueueHandler.getPreviousVISIB(STN));
        }
        if (sd.getWDSP() < 0  || sd.getWDSP() > 999.9) {
            sd.setWDSP(Main.ValidationQueueHandler.getPreviousWDSP(STN));
        }
        if (sd.getPRCP() < 0  || sd.getPRCP() > 999.99) {
            sd.setPRCP(Main.ValidationQueueHandler.getPreviousPRCP(STN));
        }
        if (sd.getSNDP() < -9999.9  || sd.getSNDP() > 9999.9) {
            sd.setSNDP(Main.ValidationQueueHandler.getPreviousSNDP(STN));
        }
        if (sd.getFRSHTT() < 0 || sd.getFRSHTT() > 111111){
            sd.setFRSHTT(Main.ValidationQueueHandler.getPreviousFRSHTT(STN));
        }
        if (sd.getCLDC() < 0  || sd.getCLDC() > 99.9) {
            sd.setCLDC(Main.ValidationQueueHandler.getPreviousCLDC(STN));
        }
        if(sd.getWNDDIR() < 0 || sd.getWNDDIR() > 359){
            sd.setWNDDIR(Main.ValidationQueueHandler.getPreviousWNDDIR(STN));
        }
    }

    private void resolve(String type, String data){
        switch (type) {
            case"STN":
                id = Integer.parseInt(data);
                sd.setSTN(id);
                break;
            case"DATE":
                sd.setDATE(data);
                break;
            case"TIME":
                sd.setTIME(data);
                break;
            case"TEMP":
                sd.setTEMP(Double.parseDouble(data));
                break;
            case"DEWP":
                sd.setDEWP(Double.parseDouble(data));
                break;
            case"STP":
                sd.setSTP(Double.parseDouble(data));
                break;
            case"SLP":
                sd.setSLP(Double.parseDouble(data));
                break;
            case"VISIB":
                sd.setVISIB(Double.parseDouble(data));
                break;
            case"WDSP":
                sd.setWDSP(Double.parseDouble(data));
                break;
            case"PRCP":
                sd.setPRCP(Double.parseDouble(data));
                break;
            case"SNDP":
                sd.setSNDP(Double.parseDouble(data));
                break;
            case"FRSHTT":
                sd.setFRSHTT(Integer.parseInt(data));
                break;
            case"CLDC":
                sd.setCLDC(Double.parseDouble(data));
                break;
            case"WNDDIR":
                sd.setWNDDIR(Integer.parseInt(data));
                break;
            default:
                break;
        }
    }
}
