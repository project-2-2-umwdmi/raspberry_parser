import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    private static final int Port = 7790;
    private static final int maxnrofConnections=5;

    static Semaphore Limiter = new Semaphore(maxnrofConnections);

    public static void main(String[] args) {
        Socket connection;
        try {
            ServerSocket receiver = new ServerSocket(Port);
            while (true) {
                connection = receiver.accept();
                Thread Worker = new Thread(new Worker(connection));
                Worker.start();
            }
        }

        catch (java.io.IOException ignored) { }
    }
}
