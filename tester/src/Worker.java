import java.net.*;
import java.io.*;

class Worker implements Runnable {
    private Socket connection;

    Worker(Socket connection) {
        this.connection = connection;
    }

    public void run() {
        try {
            Main.Limiter.p();

            BufferedReader bin = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String s;
            int cnt =0;

            while ((s = bin.readLine()) != null) {
                cnt++;
                System.out.println(s);
            }
            connection.close();
            Main.Limiter.v();
        }
        catch (IOException | InterruptedException ignored) { }
    }
}
